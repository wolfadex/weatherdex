var TempUnit = {
    Fahrenheit : 'F',
    Celsius : 'C'
};

var currentLocationWeatherData;             // The data of the current GeoLocation city
var currentUnits = TempUnit.Fahrenheit;     // The temperature units of the current GeoLocation city
var currentForecastAmount = 5;              // The forecast distance of the current GeoLocation city
var currentLocationForecastData;            // The foecast data for the current location
var otherLocationsWeatherData = [];         // The data of the user defined cities
var otherUnits =[];                         // The temperature units of the user defined cities
var otherForecastAmounts = [];              // The forecast distance of the user defined cities
var otherLocationsForecastData = [];        // The forecast data for the user defined cities

var dayColor = '#BEFFFD';                   // The color of the background during the day
var nightColor = 'rgb(41, 41, 41)';                 // The color of the background during the night
var cloudyColor = '#A5A5A5';                // The color of the background when cloudy during the day
var buttonFadeAlpha = 0.5;                  // The opacity of the buttons when not hovered over
var weatherIndex = -1;                      // The currently viewed city (-1 = GeoLocation, 0+ = other locations)
var updateWeather = false;
var addCityButtonNormal = 'Add City';
var addCityButtonLoading = 'Loading...';
var menuOpen = false;

$(document).ready(function() {
    var actionButtons = $('.actionbutton'),
        settingsButtons = $('.settingsbutton'),
        searchButtons = $('.searchbutton');

    init();

    // Regular buttons
    actionButtons.mouseenter(function() {
        var button = $(this);

        button.fadeTo('fast', 1.0);
    });

    actionButtons.mouseleave(function() {
        var button = $(this);

        button.fadeTo('fast', buttonFadeAlpha);
    });

    actionButtons.mousedown(function() {
        var button = $(this);

        if (button[0].id === 'searchbutton') {
            showSearch();
        }
        else if (button[0].id === 'settingsbutton') {
            showSettings();
        }
        else if (button[0].id === 'locationprevious') {
            previousCity();
        }
        else if (button[0].id === 'locationnext') {
            nextCity();   
        }
    });

    // Settings buttons
    settingsButtons.mouseenter(function() {
        var button = $(this);

        button.css('color','white');
    });

    settingsButtons.mouseleave(function() {
        var button = $(this);

        button.css('color','gray');
    });

    settingsButtons.mousedown(function() {
        var button = $(this);

        if (button[0].id === 'tempunits') {
            toggleTempUnits();
        }
        else if (button[0].id === 'forecastamount') {
            toggleForecastAmount();
        }
        else if (button[0].id === 'deletelocation') {
            removeCity();
        }
        else if (button[0].id === 'closesettings') {
            hideSettings();
        }
    });

    // Search buttons
    searchButtons.mouseenter(function() {
        var button = $(this);

        if (button[0].textContent !== addCityButtonLoading)
        {
            button.css('color','white');
        }
    });

    searchButtons.mouseleave(function() {
        var button = $(this);

        button.css('color','gray');
    });

    searchButtons.mousedown(function() {
        var button = $(this);

        if (button[0].id === 'addcitybutton' && button[0].textContent === addCityButtonNormal) {
            addCity($('#searchbar')[0].value)
        }
        else if (button[0].id === 'closesearch') {
            hideSearch();
        }
    });
});

function init() {
    $('#tempunits').hide();
    $('#forecastamount').hide();
    $('#deletelocation').hide();
    $('#closesettings').hide();
    $('#searchtitle').hide();
    $('#searchbar').hide();
    $('.searchbutton').hide();
    $('.actionbutton').fadeTo('fast', buttonFadeAlpha);
    $('#locationprevious').css('visibility','hidden');
    $('#locationnext').css('visibility','hidden');

    setWeatherByGeoLocation();

    setInterval(function() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(updateLocalWeather);
        }

        updateOtherWeather();
        updateOtherForecast();
    }, 1800000);
};

function updateLocalWeather(position) {
    currentLocation = position.coords;

    $.getJSON('http://api.openweathermap.org/data/2.5/weather?lat=' + currentLocation.latitude + '&lon=' + currentLocation.longitude +
              '&mode=json', function(data, status) {
        if (status === 'success') {
            currentLocationWeatherData = data;

            if (weatherIndex === -1)
            {
                setCurrentWeather(data, currentUnits);
            }
        }
    });

    updateLocalForecast(position);
};

function updateLocalForecast() {
    $.getJSON('http://api.openweathermap.org/data/2.5/forecast/daily?lat=' + currentLocation.latitude + '&lon=' +
              currentLocation.longitude + '&cnt=14&mode=json', function(data, status) {
        if (status == 'success') {
            currentLocationForecastData = data;

            if (weatherIndex === -1)
            {
                setCurrentForecast(data, currentUnits, currentForecastAmount);
            }
        }
    });
};

function updateOtherWeather() {
    $.each(otherLocationsWeatherData, function(index, location) {
        $.getJSON('http://api.openweathermap.org/data/2.5/weather?q=' + location.name + '&mode=json', function(data, status) {
            if (status === 'success') {
                otherLocationsWeatherData[weatherIndex] = data;

                if (weatherIndex === index)
                {
                    setCurrentWeather(data, otherUnits[weatherIndex]);
                }
            }
        });
    });
};

function updateOtherForecast() {
    $.each(otherLocationsWeatherData, function(index, location) {
        $.getJSON('http://api.openweathermap.org/data/2.5/forecast/daily?q=' + location.name + '&cnt=14&mode=json', function(data, status) {
            if (status == 'success') {
                otherLocationsForecastData[weatherIndex] = data;

                if (weatherIndex === index)
                {
                    setCurrentForecast(data, otherUnits[weatherIndex], otherForecastAmounts[weatherIndex]);
                }
            }
        });
    });
};

function setWeatherByGeoLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(getLocalWeather, locationError);
    }
    else {
        $('#city').html('Location access\nnot available.');
    }
};

function locationError(error) {
    var errorReason; 

    switch (error.code) {
        case error.PERMISSION_DENIED:
            errorReason = 'User denied\nlocation access.';
            break;
        case error.POSITION_UNAVAILABLE:
            errorReason = 'Location information\nis unavailable.';
            break;
        case error.TIMEOUT:
            errorReason = 'Location request timed out,\ncheck internet connection.';
            break;
        case error.UNKNOWN_ERROR:
            errorReason = 'Error Unknown:\nreport to wolfadex@gmail.com';
            break;
    }

    $('#city').html(errorReason);
};

function getLocalWeather(position) {
    currentLocation = position.coords;

    $.getJSON('http://api.openweathermap.org/data/2.5/weather?lat=' + currentLocation.latitude + '&lon=' + currentLocation.longitude +
              '&mode=json', function(data, status) {
        if (status === 'success') {
            currentLocationWeatherData = data;
            setCurrentWeather(data, currentUnits);
        }
    });

    getLocalForecast(position);
};

function getLocalForecast(position) {
    $.getJSON('http://api.openweathermap.org/data/2.5/forecast/daily?lat=' + currentLocation.latitude + '&lon=' +
              currentLocation.longitude + '&cnt=14&mode=json', function(data, status) {
        if (status == 'success') {
            currentLocationForecastData = data;
            setCurrentForecast(data, currentUnits, currentForecastAmount);
        }
    });
};

function setCurrentWeather(data, units) {
    var sunrise,
        sunset,
        currentTime = new Date(),
        isCloudy = false;

    // Data
    $('#city').html(data.name);
    $('#currenttemp').html(buildTemperatureString(data.main.temp, units));
    $('#temphi').html(buildTemperatureString(data.main.temp_max, units));
    $('#templow').html(buildTemperatureString(data.main.temp_min, units));

    // Background
    // Clouds
    if (data.clouds.all > 50) {
        isCloudy = true;
        $('#clouds').css('visibility', 'visible');
    }
    else {
        $('#clouds').css('visibility', 'hidden');
    }

    // Rain
    if ((data.rain) &&((data.rain['1h'] && data.rain['1h'] > 0) || (data.rain['2h'] && data.rain['2h'] > 0) ||
        (data.rain['3h'] && data.rain['3h'] > 0))) {
        $('#rainimage').css('visibility', 'visible');
    }
    else {
        $('#rainimage').css('visibility', 'hidden');   
    }

    // Time of day
    if (data.sys && data.sys.sunrise) {
        sunrise = new Date(data.sys.sunrise * 1000);
        sunset = new Date(data.sys.sunset * 1000);

        // Daytime
        if (currentTime > sunrise && currentTime < sunset) {
            $('#sunimage').css('visibility','visible');
            $('#moonimage').css('visibility','hidden');
            $('#stars').css('visibility','hidden');

            if (isCloudy) {
                $('body').css('background-color', cloudyColor);
            }
            else {
                $('body').css('background-color', dayColor);
            }
        }
        // Night
        else {
            $('#sunimage').css('visibility','hidden');
            $('#moonimage').css('visibility','visible');
            $('#stars').css('visibility','visible');
            $('body').css('background-color', cloudyColor);
        }
    }

    // Snow
    if ((data.snow) && ((data.snow['1h'] && data.snow['1h'] > 0) || (data.snow['2h'] && data.snow['2h'] > 0) ||
        (data.snow['3h'] && data.snow['3h'] > 0))) {
        $('#snowimage').css('visibility', 'visible');
    }
    else {
        $('#snowimage').css('visibility', 'hidden');   
    }
};

function setCurrentForecast(data, units, amount) {
    var forecastTiles = '',
        tileDate = new Date(),
        index = 0;

    // Remove the old forecast tiles.
    $('.forecasttile').remove();

    // Add the new forecast tiles.
    for (var i = 0; i < amount; i++) {
        forecastTiles += '<div class="forecasttile" style="width: 140px">';
        tileDate.setDate(tileDate.getDate() + 1);
        forecastTiles += '<div class="tiletitle">' + monthToString(tileDate.getMonth()) + ' ' + tileDate.getDate() + '</div>';
        forecastTiles += '<div class="temperaturehi">' + buildTemperatureString(data.list[index].temp.max, units) + '</div>';
        forecastTiles += '<div class="temperaturelow">' + buildTemperatureString(data.list[index].temp.min, units) + '</div>';
        forecastTiles += '</div>';
        index++;
    }

    $('#tilescontainer').html(forecastTiles);
};

function previousCity() {
    weatherIndex--;

    if (weatherIndex < -1) {
        weatherIndex = -1;
    }

    if (weatherIndex === -1) {
        $('#locationprevious').css('visibility','hidden');
        setCurrentWeather(currentLocationWeatherData, currentUnits);
        setCurrentForecast(currentLocationForecastData, currentUnits, currentForecastAmount);

        if (otherLocationsWeatherData.length > 0) {
            $('#locationnext').css('visibility','visible');
        }
    }
    else {
        $('#locationnext').css('visibility','visible');
        setCurrentWeather(otherLocationsWeatherData[weatherIndex], otherUnits[weatherIndex]);
        setCurrentForecast(otherLocationsForecastData[weatherIndex], otherUnits[weatherIndex], otherForecastAmounts[weatherIndex]);
    }
};

function nextCity() {
    weatherIndex++;

    if (weatherIndex >= otherLocationsWeatherData.length - 1) {
        $('#locationnext').css('visibility','hidden');
        weatherIndex = otherLocationsWeatherData.length - 1;
    }

    $('#locationprevious').css('visibility','visible');

    setCurrentWeather(otherLocationsWeatherData[weatherIndex], otherUnits[weatherIndex]);
    setCurrentForecast(otherLocationsForecastData[weatherIndex], otherUnits[weatherIndex], otherForecastAmounts[weatherIndex]);
};

function toggleTempUnits() {
    if (weatherIndex === -1) {
        if (currentUnits === TempUnit.Fahrenheit) {
            currentUnits = TempUnit.Celsius;
        }
        else {
            currentUnits = TempUnit.Fahrenheit;
        }

        $('#tempunits')[0].textContent = 'Units: ' + currentUnits;
    }
    else {
        if (otherUnits[weatherIndex] === TempUnit.Fahrenheit) {
            otherUnits[weatherIndex] = TempUnit.Celsius;
        }
        else {
            otherUnits[weatherIndex] = TempUnit.Fahrenheit;
        }
        
        $('#tempunits')[0].textContent = 'Units: ' + otherUnits[weatherIndex];
    }

    updateWeather = true;
};

function toggleForecastAmount() {
    if (weatherIndex === -1) {
        if (currentForecastAmount === 5) {
            currentForecastAmount = 14;
        }
        else {
            currentForecastAmount = 5;
        }
        
        $('#forecastamount')[0].textContent = 'Forecast: ' + currentForecastAmount + ' Days';
    }
    else {
        if (otherForecastAmounts[weatherIndex] === 5) {
            otherForecastAmounts[weatherIndex] = 14;
        }
        else {
            otherForecastAmounts[weatherIndex] = 5;
        }
        
        $('#forecastamount')[0].textContent = 'Forecast: ' + otherForecastAmounts[weatherIndex] + ' Days';
    }

    updateWeather = true;
};

function hideSearch() {
    $('#searchtitle').hide();
    $('#searchbar').hide();
    $('.searchbutton').hide();
    $('#addcitybutton')[0].textContent = addCityButtonNormal;

    $('#searchwindow').animate({height:"0"});

    menuOpen = false;
};

function showSearch() {
    $('#searchwindow').animate({
        height: '60%'
    }, {
        queue: false,
        complete: searchOpen
    });

    menuOpen = true;
};

function searchOpen() {
    $('#searchtitle').show();
    $('#searchbar').show();
    $('.searchbutton').show();
};

function hideSettings() {
    $('#tempunits').hide();
    $('#forecastamount').hide();
    $('#deletelocation').hide();
    $('#closesettings').hide();

    $('#settingswindow').animate({height:"0"});

    if (updateWeather) {
        if (weatherIndex === -1)
        {
            setCurrentWeather(currentLocationWeatherData, currentUnits);
            setCurrentForecast(currentLocationForecastData, currentUnits, currentForecastAmount);
        }
        else {
            setCurrentWeather(otherLocationsWeatherData[weatherIndex], otherUnits[weatherIndex]);
            setCurrentForecast(otherLocationsForecastData[weatherIndex], otherUnits[weatherIndex], otherForecastAmounts[weatherIndex]);
        }

        updateWeather = false;
    }

    menuOpen = false;
};

function showSettings() {
    $('#settingswindow').animate({
        height: '60%'
    }, {
        queue: false,
        complete: settingsOpen
    });

    menuOpen = true;
};

function settingsOpen() {
    if (weatherIndex === -1) {
        $('#tempunits')[0].textContent = 'Units: ' + currentUnits;
        $('#forecastamount')[0].textContent = 'Forecast: ' + currentForecastAmount + ' Days';
    }
    else {
        $('#tempunits')[0].textContent = 'Units: ' + otherUnits[weatherIndex];
        $('#forecastamount')[0].textContent = 'Forecast: ' + otherForecastAmounts[weatherIndex] + ' Days';
    }

    $('#tempunits').show();
    $('#forecastamount').show();
    $('#closesettings').show();

    if (weatherIndex > -1) {
        $('#deletelocation').show();
    }
};

function buildTemperatureString(temperature, units) {
    var degrees = '&deg;',
        temp = (temperature === undefined) ? 0 : temperature,
        resultTemp;

    if (units === TempUnit.Fahrenheit) {
        degrees += TempUnit.Fahrenheit;
        resultTemp = kelvinToFahrenheit(temp).toFixed(0) + degrees;
    }
    else {
        degrees += TempUnit.Celsius;
        resultTemp = kelvinToCelsius(temp).toFixed(0) + degrees;
    }

    return resultTemp;
};

function addCity(cityName) {
    $('#addcitybutton')[0].textContent = addCityButtonLoading;

    $.getJSON('http://api.openweathermap.org/data/2.5/weather?q=' + cityName + '&mode=json', function(data, status) {
        if (status === 'success') {
            if (data.message !== undefined) {
                $('#searchbar')[0].value = "No Such City";
                $('#addcitybutton')[0].textContent = addCityButtonNormal;
            }
            else {
                if (data.name === '') {
                    $('#searchbar')[0].value = $('#searchbar')[0].value[0].toUpperCase() + $('#searchbar')[0].value.slice(1).toLowerCase();
                    data.name = $('#searchbar')[0].value;
                }

                otherLocationsWeatherData.push(data)
                
                if (weatherIndex === -1) {
                    otherUnits.push(currentUnits);
                    otherForecastAmounts.push(currentForecastAmount);
                }
                else {
                    otherUnits.push(otherUnits[weatherIndex]);
                    otherForecastAmounts.push(otherForecastAmounts[weatherIndex]);
                }

                $('#locationprevious').css('visibility','visible');
                $('#locationnext').css('visibility','hidden');
                weatherIndex = otherLocationsWeatherData.length - 1;
                setCurrentWeather(data, otherUnits[weatherIndex]);

                getCityForecast(cityName);
            }
        }
    });
};

function removeCity() {
    otherLocationsWeatherData.splice(weatherIndex, 1);
    otherUnits.splice(weatherIndex, 1);
    otherForecastAmounts.splice(weatherIndex, 1);
    otherLocationsForecastData.splice(weatherIndex, 1);

    weatherIndex--;

    if (weatherIndex === -1) {
        $('#locationprevious').css('visibility','hidden');

        setCurrentWeather(currentLocationWeatherData, currentUnits);
        setCurrentForecast(currentLocationForecastData, currentUnits, currentForecastAmount);
    }
    else {
        setCurrentWeather(otherLocationsWeatherData[weatherIndex], otherUnits[weatherIndex]);
        setCurrentForecast(otherLocationsForecastData[weatherIndex], otherUnits[weatherIndex], otherForecastAmounts[weatherIndex]);
    }

    hideSettings();
};

function getCityForecast(cityName) {
    $.getJSON('http://api.openweathermap.org/data/2.5/forecast/daily?q=' + cityName + '&cnt=14&mode=json', function(data, status) {
        if (status == 'success') {
            otherLocationsForecastData.push(data);
            setCurrentForecast(data, otherUnits[weatherIndex], otherForecastAmounts[weatherIndex]);

            hideSearch();
        }
    });
};

/*
 * Temperature conversion formulas.
 * All math based on the formulas at
 * http://en.wikipedia.org/wiki/Conversion_of_units_of_temperature
 */
function kelvinToCelsius(kelvin) {
    return kelvin - 273.15;
};

function kelvinToFahrenheit(kelvin) {
    return kelvin * 1.8 - 459.67;
};

function celsiusToFahrenheit(celsius) {
    return (celsius * 1.8) + 32;
};

function celsiusToKelvin(celsius) {
    return celsius + 273.15;
};

function fahrenheitToCelsius(fahrenheit) {
    return (fahrenheit - 32) * 5 / 9;
};

function fahrenheitToKelvin(fahrenheit) {
    return (fahrenheit + 459.67) * 5 / 9
};

/*
 *
 */
function monthToString(month) {
    var returnMonth = '';

    switch (month) {
        case 0:
            returnMonth = 'January';
            break;
        case 1:
            returnMonth = 'February';
            break;
        case 2:
            returnMonth = 'March';
            break;
        case 3:
            returnMonth = 'April';
            break;
        case 4:
            returnMonth = 'May';
            break;
        case 5:
            returnMonth = 'June';
            break;
        case 6:
            returnMonth = 'July';
            break;
        case 7:
            returnMonth = 'August';
            break;
        case 8:
            returnMonth = 'September';
            break;
        case 9:
            returnMonth = 'October';
            break;
        case 10:
            returnMonth = 'November';
            break;
        case 11:
            returnMonth = 'December';
            break;
    }

    return returnMonth;
};
