Weatherdex
-------------------------------------------

author: Wolfgang Schuster
version: 1.0.0
date: 20th of May 2014

-------------------------------------------
How To Install:
1. Download and install Node.js from nodejs.org.
2. Unzip the contents of this folder onto the machine that will act as the server.

-------------------------------------------
How To Run:
1. Open a command prompt and navigate to the unzipped folder.
2. Enter the text 'npm start' and press return.
    - If you get 'npm is not recognized as an internal or external command', then try restarting your computer.
3. In your browser, go to the address 'http://ipaddress:port' where:
    ipaddress = the ip address of the server
    port = the open port specified (defaults to 3000)

-------------------------------------------
Other:
To change the port, navigate to bin and open the file WWW with any text editor.
Then change line 6 to the desired port number (defaults to 3000).